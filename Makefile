test:
	go test -v ./... -cover -coverprofile c.out

run/example_http:
	go run ./cmd/example_http

run/example_https:
	go run ./cmd/example_https

run/example_events:
	go run ./cmd/example_events

run/example_https_invalid:
	go run ./cmd/example_https_invalid

run/example_shutdown:
	go run ./cmd/example_shutdown

bin/-:
	@$(MAKE) -B bin/example_events
	@$(MAKE) -B bin/example_http
	@$(MAKE) -B bin/example_https
	@$(MAKE) -B bin/example_https_invalid
	@$(MAKE) -B bin/example_shutdown

bin/example_events:
	go build -o bin/example_events ./cmd/example_events
	CGO_ENABLED=0 go build -a -ldflags '-extldflags "static"' -o bin/example_events_static ./cmd/example_events

bin/example_http:
	go build -o bin/example_http ./cmd/example_http
	CGO_ENABLED=0 go build -a -ldflags '-extldflags "static"' -o bin/example_http_static ./cmd/example_http

bin/example_https:
	go build -o bin/example_https ./cmd/example_https
	CGO_ENABLED=0 go build -a -ldflags '-extldflags "static"' -o bin/example_https_static ./cmd/example_https

bin/example_https_invalid:
	go build -o bin/example_https_invalid ./cmd/example_https_invalid
	CGO_ENABLED=0 go build -a -ldflags '-extldflags "static"' -o bin/example_https_invalid_static ./cmd/example_https_invalid

bin/example_shutdown:
	go build -o bin/example_shutdown ./cmd/example_shutdown
	CGO_ENABLED=0 go build -a -ldflags '-extldflags "static"' -o bin/example_shutdown_static ./cmd/example_shutdown

assets/tls/-:
	@$(MAKE) assets/tls
	mv assets/tls/server.crt assets/tls/server2.crt;
	mv assets/tls/server.key assets/tls/server2.key;
	@$(MAKE) -B assets/tls

assets/tls:
	@mkdir -p ./assets/tls
	@openssl genrsa -out ./assets/tls/server.key 2048
	@openssl req -new -x509 -sha256 \
		-days 3650 \
		-key ./assets/tls/server.key \
		-out ./assets/tls/server.crt \
		-subj "/C=SG/ST=Singapore/L=Singapore/O=zephinzer/CN=localhost"

# use this to generate the deploy keys
.ssh:
	@mkdir -p ./.ssh
	@ssh-keygen -t rsa -b 4096 -f ./.ssh/id_rsa -N ""
	@cat ./.ssh/id_rsa | base64 -w 0 > ./.ssh/id_rsa.b64
