package utils

import (
	"bytes"
	"os"
)

func ConcatStrings(strings ...string) string {
	var b bytes.Buffer
	length := len(strings)
	for i := 0; i < length; i++ {
		b.WriteString(strings[i])
	}
	return b.String()
}

func FileExists(pathToFile string) bool {
	fileInfo, err := os.Lstat(pathToFile)
	if err != nil {
		return false
	}
	return !fileInfo.IsDir() &&
		(fileInfo.Mode()&os.ModeSymlink == 0)
}
