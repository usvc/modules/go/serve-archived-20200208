package example

import (
	"log"
	"net/http"
)

func HelloWorldHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("hello world"))
}

func RequestReceivedLoggerMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("[received request] %s %s %s%s\n", r.Proto, r.Method, r.Host, r.URL)
		handler.ServeHTTP(w, r)
	})
}
