module github.com/usvc/go-serve

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	github.com/phayes/freeport v0.0.0-20180830031419-95f893ade6f2
	github.com/stretchr/testify v1.4.0
)
