# Server

This repository contains packages related to starting/running a server destined for deployment in a cloud-native architecture. The following were taken into consideration:

1. Handlers
2. Middlewares
3. Graceful shutdown handling
4. Event sourcing of server events

# Usage

## Importing

```go
import (
  // for the server
  "github.com/usvc/go-serve/pkg/server"
)
```

## Server Initialization

To create a HTTP server instance:

```go
s := server.New(&server.Config{
  Port:      server.DefaultPort,
  Interface: server.DefaultInterface,
})
```

To create a HTTPS server instance, specify the paths to the TLS certificate and key:

```go
s := server.New(&server.Config{
  Port:        server.DefaultPort,
  Interface:   server.DefaultInterface,
  TLSCertPath: "./assets/tls/server.crt",
  TLSKeyPath:  "./assets/tls/server.key",
})
```

## Using the event log

The server instance utilises an event log to send server events to, but it is not enabled by default. To enable it, use the following after initializing the server:

```go
s.InitializeEvents()
go func() {
  for {
    select {
    case event := <-s.Events:
      log.Println(event)
    }
  }
}()
```

## Adding a path handler

The following adds an endpoint handler for `"/"` that simply responds with `"hello world"`

```go
s.AddHandler("/", func(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("hello world"))
})
```

## Adding a middleware

The following adds a request logger middleware:

```go
s.AddMiddleware(func(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("[received request] %s %s %s%s\n", r.Proto, r.Method, r.Host, r.URL)
		handler.ServeHTTP(w, r)
	})
})
```

## Starting the server

After you've added the required handlers and middlewares, start the server with:

```go
s.Start()
```

# Examples

Examples applications can be found in the [`./cmd` directory](./cmd):

- [With HTTP](./cmd/example_http)
- [With Events Log](./cmd/example_events)
- [With HTTPS](./cmd/example_https)
- [With Invalid HTTPS](./cmd/example_https_invalid)

# Packages

## `github.com/usvc/go-serve/pkg/server`

Contains the main server instance.

### `Config`

Properties:
- **Port**: Specifies the port to listen on
- **Interface**: Specifies the interface to bind to
- **TLSKeyPath**: Specifies the path to the TLS key to enable HTTPS
- **TLSCertPath**: Specifies the path to the TLS certificate to enable HTTPS

### `Server`

#### `AddHandler`

#### `AddHandlers`

#### `AddMiddleware`

#### `GetAddr`

#### `GetHandlers`

#### `GetMiddlewares`

#### `InitializeEvents`

#### `Start`

# Development Runbook

## Assets Initialization

The example applications require that the `./assets/tls` directory be populated with 2 sets of server keys/certs. To do this, run:

```sh
make assets/tls/-;
```

You should have 2 sets of server keys and certificates for the `example_https_invalid` to demonstrate the behaviour when an invalid key/cert pair is provided.

**NOTE**: You will need these 2 sets of matching keys and certificates for the tests to run.

## Manual Testing

To verify things are working as expected, the Makefile contains several recipes beginning with `run/` that you can use to do integration tests:

```sh
make run/example_events;
make run/example_http;
make run/example_https;
make run/example_https_invalid;
make run/example_shutdown;
```

## Automated Testing

Unit tests are available. To run them, use the `test` Makefile recipe:

```sh
make test;
```


## Releasing to GitHub

The GitHub URL for this repository is [https://github.com/usvc/go-log](https://github.com/usvc/go-log). The pipeline is configured to automatically push to this repository. Should the keys need to be regenerated, the `.ssh` Makefile recipe contains the commands required to generate the keys in a `.ssh` directory:

```sh
make .ssh
```

Inside the `.ssh` directory, copy the contents of `id_rsa.b64` and paste it as the `DEPLOY_KEY` CI/CD variable. Then copy the contents of `id_rsa.pub` and paste that as a deploy key with write access in the GitHub repository.


## Continuous integration/delivery (CI/CD) pipeline configuration

The following environment variables should be set in the CI/CD settings under Variables:

| Key | Description | Example |
| --- | --- | --- |
| `DEPLOY_KEY` | The base64 encoded private key that corresponds to the repository URL specified in `NEXT_REPO` | *(Output of `cat ~/.ssh/id_rsa \| base64 -w 0`)* |
| `GITHUB_URL` | The SSH clone URL of the repository to push to in the `release` stage of the pipeline | `git@github.com:usvc/go-serve.git` |


- - -


# License

This project is licensed under the MIT license. [See the full text here](./LICENSE).
