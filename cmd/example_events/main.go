package main

import (
	"log"

	"github.com/usvc/go-serve/lib/example"
	"github.com/usvc/go-serve/pkg/server"
)

func main() {
	s := server.New(&server.Config{
		Port:      server.DefaultPort,
		Interface: server.DefaultInterface,
	})

	// initialize events for demonstration
	s.InitializeEvents()
	go func() {
		for {
			select {
			case event := <-s.Events:
				log.Println(event)
			}
		}
	}()

	// adds a handler for the path at '/'
	s.AddHandler("/", example.HelloWorldHandler)

	// adds a logger middleware that simply displays request received
	s.AddMiddleware("logger", "logs incoming requests", example.RequestReceivedLoggerMiddleware)

	// start the server
	s.Start()
}
