package main

import (
	"log"
	"os"
	"os/signal"

	"github.com/usvc/go-serve/lib/example"
	"github.com/usvc/go-serve/pkg/server"
)

func main() {
	done := make(chan bool)
	s := server.New(&server.Config{
		Port:      server.DefaultPort,
		Interface: server.DefaultInterface,
	})

	// initialize events for demonstration
	s.InitializeEvents()
	go func() {
		for {
			select {
			case event := <-s.Events:
				log.Println(event)
				if event.Code == server.EventShutdown {
					done <- true
				}
			}
		}
	}()

	// adds a handler for the path at '/'
	s.AddHandler("/", example.HelloWorldHandler)

	// adds a logger middleware that simply displays request received
	s.AddMiddleware("logger", "logs incoming requests", example.RequestReceivedLoggerMiddleware)

	s.AddShutodownHandler(func() {
		log.Println("shutting down")

	})

	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, os.Interrupt, os.Kill)
	go func() {
		for _ = range signalChannel {
			s.Stop()
		}
	}()

	// start the server
	s.Start()
	<-done
}
