package server

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

func TestConfig(t *testing.T) {
	suite.Run(t, &ConfigTests{})
}

type ConfigTests struct {
	suite.Suite
}

func (s *ConfigTests) Test_DefaultPort() {
	s.True(
		DefaultPort < 65535 && DefaultPort > 1000,
		"DefaultPort is in the range of non-privileged ports",
	)
}

func (s *ConfigTests) Test_DefaultInterface() {
	zeroToTwoFiveFive := "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
	regex := fmt.Sprintf("^(%s.){3}%s$", zeroToTwoFiveFive, zeroToTwoFiveFive)
	s.Regexp(
		regex,
		DefaultInterface,
		"DefaultInterface is a valid IP address",
	)
}

func (s *ConfigTests) Test_DefaultMaxHeaderBytes() {
	s.False(
		DefaultMaxHeaderBytes < 32*KiloByte,
		"DefaultMaxHeaderBytes is less than 32 kilobytes, this is too little",
	)
	s.False(
		DefaultMaxHeaderBytes > 512*KiloByte,
		"DefaultMaxHeaderBytes is more than 512 kilobytes, this might cause your server to be overloaded",
	)
}

func (s *ConfigTests) Test_DefaultTimeoutIdle() {
	s.False(
		DefaultTimeoutIdle < 5*time.Second,
		"DefaultTimeoutIdle is less than 5 seconds, this is too little",
	)
	s.False(
		DefaultTimeoutIdle > time.Minute,
		"DefaultTimeoutIdle is more than a minute, this might cause your server to be overloaded with requests",
	)
}

func (s *ConfigTests) Test_DefaultTimeoutRead() {
	s.False(
		DefaultTimeoutRead < 3*time.Second,
		"DefaultTimeoutRead is less than 3 seconds, this is too little",
	)
	s.False(
		DefaultTimeoutRead > 30*time.Second,
		"DefaultTimeoutRead is more than a minute, this might cause your server to be overloaded with requests",
	)
}

func (s *ConfigTests) Test_DefaultTimeoutReadHeader() {
	s.False(
		DefaultTimeoutReadHeader < 3*time.Second,
		"DefaultTimeoutReadHeader is less than 3 seconds, this is too little",
	)
	s.False(
		DefaultTimeoutReadHeader > 30*time.Second,
		"DefaultTimeoutReadHeader is more than a minute, this might cause your server to be overloaded with requests",
	)
	s.False(
		DefaultTimeoutReadHeader > DefaultTimeoutRead,
		"DefaultTimeoutReadHeader is more than DefaultTimeoutRead, this might cause issues",
	)
}

func (s *ConfigTests) Test_DefaultTimeoutWrite() {
	s.False(
		DefaultTimeoutWrite < 3*time.Second,
		"DefaultTimeoutWrite is less than 3 seconds, this might cause timeout of valid response times for larger requests",
	)
	s.False(
		DefaultTimeoutWrite > 30*time.Second,
		"DefaultTimeoutWrite is more than a minute, this might cause your server to be overloaded with writing responses",
	)
}

func (s *ConfigTests) Test_NewConfig() {
	config := NewConfig()
	s.Equal(DefaultInterface, config.Interface)
	s.Equal(DefaultPort, config.Port)
	s.Equal(DefaultMaxHeaderBytes, config.MaxHeaderBytes)
	s.Equal(DefaultTimeoutIdle, config.TimeoutIdle)
	s.Equal(DefaultTimeoutRead, config.TimeoutRead)
	s.Equal(DefaultTimeoutReadHeader, config.TimeoutReadHeader)
	s.Equal(DefaultTimeoutWrite, config.TimeoutWrite)
}
