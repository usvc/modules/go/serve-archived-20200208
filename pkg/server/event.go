package server

import (
	"fmt"
)

// EventExitted defines the Event.Code value for when the
// Server instance has exitted
const EventExitted = "SERVER_EXITTED"

// EventErrored defines the Event.Code value for when an
// error happens to the Server
const EventErrored = "SERVER_ERRORED"

// EventHello defines the Event.Code value for when the
// Server starts logging to an event channel
const EventHello = "SERVER_EVENT_HELLO"

// EventHandlerAdded defines the Event.Code value for when
// a new handler is added
const EventHandlerAdded = "SERVER_HANDLER_ADDED"

// EventMiddlewareAdded defines the Event.Code value for when
// a new middleware is added
const EventMiddlewareAdded = "SERVER_MIDDLEWARE_ADDED"

// EventShutdown defines the Event.Code value for when the
// Server is shutting down
const EventShutdown = "SERVER_SHUTDOWN"

// EventShutdownHandlerAdded defines the Event.Code value for when the
// Server has a new shutdown handler
const EventShutdownHandlerAdded = "SERVER_SHUTDOWN_HANDLER_ADDED"

// EventStartingWithoutTLS defines the Event.Code value for when the
// Server is starting up without TLS
const EventStartingWithoutTLS = "SERVER_STARTING_WITHOUT_TLS"

// EventStartingWithTLS defines the Event.Code value for when the
// Server is starting up with TLS
const EventStartingWithTLS = "SERVER_STARTING_WITH_TLS"

// EventTLSEnabledCheck defines the Event.Code value when the
// Server is checking if TLS configurations have been set
const EventTLSEnabledCheck = "SERVER_TLS_ENABLED_CHECK"

// EventTLSValidityCheck defines the Event.Code value when the
// Server is checking if the TLS configurations are valid
const EventTLSValidityCheck = "SERVER_TLS_VALIDITY_CHECK"

// Event stores server events
type Event struct {
	Code    string
	Message string
}

// Error ensures that the Event remains
func (event Event) Error() string {
	return fmt.Sprintf("[%s] %s", event.Code, event.Message)
}
