package server

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

func TestEvents(t *testing.T) {
	suite.Run(t, &EventTests{})
}

type EventTests struct {
	suite.Suite
}

func (s *EventTests) TestError_whenPrinting() {
	ev := Event{"CODE", "message"}
	evText := fmt.Sprintf("%s", ev)
	s.Equal("[CODE] message", evText)
}

func (s *EventTests) TestError_withPanic() {
	defer func() {
		r := recover()
		s.NotNil(r)
		evText := fmt.Sprintf("%s", r)
		s.Equal("[CODE] message", evText)
	}()
	ev := Event{"CODE", "message"}
	panic(ev)
}
