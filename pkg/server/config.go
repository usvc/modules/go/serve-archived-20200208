package server

import "time"

const (
	Byte     = iota
	KiloByte = 1 << (10 * iota)
	MegaByte
	GigaByte
	TeraByte
)

// DefaultPort defines an arbitrary default port to listen on
const DefaultPort uint = 12345

// DefaultInterface defines the most likely interface we would want
// to listen on in a container environment
const DefaultInterface string = "0.0.0.0"

// DefaultMaxHeaderBytes defines a sensible number of bytes to accept
// in any incoming request's headers
const DefaultMaxHeaderBytes = KiloByte * 128

// DefaultTimeoutIdle defines a sensible duration to wait for idle
// connections to avoid high memory usage
const DefaultTimeoutIdle = time.Second * 10

// DefaultTimeoutRead defines a sesnsible duration to wait for the
// data to be read into the application's memory space
const DefaultTimeoutRead = time.Second * 8

// DefaultTimeoutReadHeader defines a sensible duration to wait for
// header reads
const DefaultTimeoutReadHeader = time.Second * 8

// DefaultTimeoutWrite defines a sensible duration to wait for the
// data writing operation to complete
const DefaultTimeoutWrite = time.Second * 8

// NewConfig creates a new Config structure with sensible defaults
func NewConfig() Config {
	return Config{
		Interface:         DefaultInterface,
		Port:              DefaultPort,
		MaxHeaderBytes:    DefaultMaxHeaderBytes,
		TimeoutIdle:       DefaultTimeoutIdle,
		TimeoutRead:       DefaultTimeoutRead,
		TimeoutReadHeader: DefaultTimeoutReadHeader,
		TimeoutWrite:      DefaultTimeoutWrite,
	}
}

// Config defines a structure to hold the configuration of the server
type Config struct {
	// Interface specifies the interface to bind to
	Interface string
	// Port specifies the port to listen on
	Port uint
	// MaxHeaderBytes specifies the limit of the header size in bytes
	MaxHeaderBytes int
	// TimeoutIdle specifies the timeout for idle requests
	TimeoutIdle time.Duration
	// TimeoutRead specifies the timeout for reading a request
	TimeoutRead time.Duration
	// TimeoutReadHeader specifies the timeout for reading a request headers
	TimeoutReadHeader time.Duration
	// TimeoutWrite specifies the timeout for writing responses
	TimeoutWrite time.Duration
	// TLSKeyPath specifies the path to the TLS key to enable HTTPS
	TLSKeyPath string
	// TLSCertPath specifies the path to the TLS certificate to enable HTTPS
	TLSCertPath string
}
