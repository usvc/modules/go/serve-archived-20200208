package server

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/phayes/freeport"
	"github.com/stretchr/testify/suite"
)

func TestServer(t *testing.T) {
	suite.Run(t, &ServerTests{})
}

type ServerTests struct {
	expectedHandlerFunc       func(http.ResponseWriter, *http.Request)
	expectedMiddlewareHandler func(http.Handler) http.Handler
	pathToTLSKey              string
	pathToTLSCert             string
	waitDurationServerStart   time.Duration
	suite.Suite
}

func (s *ServerTests) SetupTest() {
	s.expectedHandlerFunc = func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("hello world"))
	}
	s.expectedMiddlewareHandler = func(h http.Handler) http.Handler {
		return http.HandlerFunc(s.expectedHandlerFunc)
	}
	s.pathToTLSKey = "../../assets/tls/server.key"
	s.pathToTLSCert = "../../assets/tls/server.crt"
	s.waitDurationServerStart = 500 * time.Millisecond
}

func (s *ServerTests) TestNew() {
	config := NewConfig()
	server := New(&config)
	defer close(server.shutdown)
	s.NotNil(server.config, "populates the .config property")
	s.NotNil(server.instance, "populates the .instance property")
	s.NotNil(server.shutdown, "populates the .shutdown property")
}

func (s *ServerTests) TestAddHandler() {
	expectedHandler := Handler{
		PathURI: "/__test",
		Handler: s.expectedHandlerFunc,
	}
	config := NewConfig()
	server := New(&config)
	server.AddHandler(expectedHandler.PathURI, expectedHandler.Handler)
	s.Len(server.GetHandlers(), 1, "handler should be added to the server instance")
	s.Equal(expectedHandler.PathURI, server.GetHandlers()[0].PathURI, "specified pathURI was registered")
}

func (s *ServerTests) TestAddMiddleware() {
	expectedMiddleware := Middleware{
		Name:        "__middleware_name",
		Description: "__middleware_description",
		Handler:     s.expectedMiddlewareHandler,
	}
	config := NewConfig()
	server := New(&config)
	server.AddMiddleware(
		expectedMiddleware.Name,
		expectedMiddleware.Description,
		expectedMiddleware.Handler,
	)
	middlewares := server.GetMiddlewares()
	s.Len(middlewares, 1, "middleware should be added to the server instance")
	s.Equal(expectedMiddleware.Name, middlewares[0].Name, "specified middleware was registered with the right name")
	s.Equal(expectedMiddleware.Description, middlewares[0].Description, "specified middleware was registered with the right description")
}

func (s *ServerTests) TestAddShutdownHandler() {
	done := make(chan bool, 1)
	defer close(done)
	expectedShutdownHandler := func() {
		done <- true
	}
	config := NewConfig()
	server := New(&config)
	server.AddShutodownHandler(expectedShutdownHandler)
	handlers := server.GetShutdownHandlers()
	s.Len(handlers, 1, "shutdown handler should be added to the server instance")
	handlers[0]()
	s.Len(done, 1, "expected function to be the one added")
}

func (s *ServerTests) TestGetAddr() {
	config := NewConfig()
	config.Port = 65535
	config.Interface = "__test_interface"
	expectedAddr := fmt.Sprintf("%s:%v", config.Interface, config.Port)
	server := New(&config)
	s.Equal(expectedAddr, server.GetAddr())
}

func (s *ServerTests) TestInitializeEvents() {
	var expectedEventsBufferSize uint = 5
	config := NewConfig()
	server := New(&config)
	s.Nil(server.Events)
	server.InitializeEvents(expectedEventsBufferSize)
	defer close(server.Events)
	s.Equal(int(expectedEventsBufferSize), cap(server.Events))
}

func (s *ServerTests) TestIsTLS() {
	pathToNonExistentKey := "../../assets/tls/no.key"
	pathToNonExistentCert := "../../assets/tls/no.crt"

	config := NewConfig()
	config.TLSKeyPath = s.pathToTLSKey
	config.TLSCertPath = s.pathToTLSCert

	server := New(&config)
	s.True(server.IsTLS(), "works when the stars are aligned")

	config.TLSKeyPath = pathToNonExistentKey
	config.TLSCertPath = s.pathToTLSCert
	s.False(server.IsTLS(), "fails if key doesn't exist")

	config.TLSKeyPath = s.pathToTLSKey
	config.TLSCertPath = pathToNonExistentCert
	s.False(server.IsTLS(), "fails if cert doesn't exist")

	config.TLSKeyPath = pathToNonExistentKey
	config.TLSCertPath = pathToNonExistentCert
	s.False(server.IsTLS(), "fails if both don't exist")

	config.TLSKeyPath = s.pathToTLSKey
	config.TLSCertPath = "../../assets/tls/server2.crt"
	s.False(server.IsTLS(), "fails with mismatched keys/certs")

	config.TLSKeyPath = "../../assets/tls/server2.key"
	config.TLSCertPath = s.pathToTLSCert
	s.False(server.IsTLS(), "fails with mismatched keys/certs")
}

func (s *ServerTests) TestStart_noTLS() {
	expectedPathURI := "/__test"
	expectedResponseBody := []byte("hello world")
	port, err := freeport.GetFreePort()
	s.Nil(err)
	requestString := fmt.Sprintf("http://127.0.0.1:%v%s", port, expectedPathURI)
	config := NewConfig()
	config.Port = uint(port)
	server := New(&config)
	server.AddHandler(expectedPathURI, func(w http.ResponseWriter, r *http.Request) {
		w.Write(expectedResponseBody)
	})
	go func(after <-chan time.Time) {
		<-after
		res, err := http.Get(requestString)
		s.Nil(err)
		s.NotNil(res)
		s.Equal(http.StatusOK, res.StatusCode)
		body, err := ioutil.ReadAll(res.Body)
		s.Nil(err)
		s.Equal(expectedResponseBody, body)
		server.Stop()
	}(time.After(s.waitDurationServerStart))
	server.Start()
}

func (s *ServerTests) TestStart_withTLS() {
	expectedPathURI := "/__test"
	expectedResponseBody := []byte("hello world")
	port, err := freeport.GetFreePort()
	s.Nil(err)
	requestString := fmt.Sprintf("https://127.0.0.1:%v%s", port, expectedPathURI)
	config := NewConfig()
	config.Port = uint(port)
	config.TLSKeyPath = s.pathToTLSKey
	config.TLSCertPath = s.pathToTLSCert

	server := New(&config)
	server.AddHandler(expectedPathURI, func(w http.ResponseWriter, r *http.Request) {
		w.Write(expectedResponseBody)
	})

	go func(after <-chan time.Time) {
		defer func() {
			server.Stop()
		}()
		<-after
		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
			},
		}
		res, err := client.Get(requestString)
		s.Nil(err)
		s.NotNil(res)
		s.Equal(http.StatusOK, res.StatusCode)
		body, err := ioutil.ReadAll(res.Body)
		s.Nil(err)
		s.Equal(expectedResponseBody, body)
	}(time.After(s.waitDurationServerStart))
	server.Start()
}
