package server

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"path"
	"time"

	"github.com/gorilla/mux"
	"github.com/usvc/go-serve/lib/utils"
)

// DefaultEventsLength defines a length to set the
// number of server events to
const DefaultEventsLength uint = 128

// New initialises a new server with basic configurations
// settled for you
func New(config *Config) *Server {
	server := Server{
		config:   config,
		instance: mux.NewRouter(),
		shutdown: make(chan bool),
	}
	return &server
}

// Handler defines the structure for path handlers
type Handler struct {
	PathURI string
	Handler http.HandlerFunc
}

// Middleware defines the structure of server middlewares
type Middleware struct {
	Name        string
	Description string
	Handler     func(http.Handler) http.Handler
}

// Server holds information about the server
type Server struct {
	Events           chan Event
	config           *Config
	done             chan error
	handlers         []Handler
	instance         *mux.Router
	middlewares      []Middleware
	shutdown         chan bool
	shutdownHandlers []func()
}

// AddHandler adds a single handler to the server
func (server *Server) AddHandler(pathURI string, handler http.HandlerFunc) {
	server.logEvent(EventHandlerAdded, fmt.Sprintf("added handler for path: [%s]", pathURI))
	server.handlers = append(server.handlers, Handler{
		PathURI: pathURI,
		Handler: handler,
	})
}

// AddHandlers adds all handlers from the provided Server
// to the current; arguments after the server will be taken as
// a path segment each
func (server *Server) AddHandlers(anotherServer *Server, pathPrefixes ...string) {
	allHandlers := anotherServer.GetHandlers()
	basePath := "/"
	if len(pathPrefixes) > 0 {
		for i := 0; i < len(pathPrefixes); i++ {
			basePath = path.Join(basePath, pathPrefixes[i])
		}
	}
	for i := 0; i < len(allHandlers); i++ {
		server.AddHandler(
			path.Join(
				basePath,
				allHandlers[i].PathURI,
			),
			allHandlers[i].Handler,
		)
	}
}

// AddMiddleware adds a middleware to this server
func (server *Server) AddMiddleware(name, description string, middleware func(http.Handler) http.Handler) {
	server.logEvent(EventMiddlewareAdded, fmt.Sprintf("added middleware: [%s] - %s", name, description))
	server.middlewares = append(server.middlewares, Middleware{
		Description: description,
		Handler:     middleware,
		Name:        name,
	})
}

// AddShutodownHandler adds a shutdown handler to the server, note
// that shutdown handlers will run simultaneously
func (server *Server) AddShutodownHandler(shutdownHandler func()) {
	server.logEvent(EventShutdownHandlerAdded, "added shutdown handler")
	server.shutdownHandlers = append(server.shutdownHandlers, shutdownHandler)
}

// GetAddr returns the address that the server instance
// will be listening on
func (server *Server) GetAddr() string {
	return fmt.Sprintf("%s:%v", server.config.Interface, server.config.Port)
}

// GetHandlers returns all the handlers from the Server instance
func (server *Server) GetHandlers() []Handler {
	return server.handlers
}

// GetMiddlewares returns all the middlewares from the Server instance
func (server *Server) GetMiddlewares() []Middleware {
	return server.middlewares
}

// GetShutdownHandlers returns all the shutdown handlers from the Server instance
func (server *Server) GetShutdownHandlers() []func() {
	return server.shutdownHandlers
}

// InitializeEvents initializes the events channel, to use it, listen
// on the server.Events channel
func (server *Server) InitializeEvents(eventsBufferLength ...uint) {
	eventsBufferLen := DefaultEventsLength
	if len(eventsBufferLength) > 0 {
		eventsBufferLen = eventsBufferLength[0]
	}
	server.Events = make(chan Event, eventsBufferLen)
	server.logEvent(EventHello, "hola")
}

// IsTLS returns whether the server to be spun up will
// have TLS enabled
func (server *Server) IsTLS() bool {
	return server.isTLSEnabled() && server.isTLSValid()
}

// Start the Server asynchronously
func (server *Server) Start() {
	instance := server.createServerInstance()
	if server.IsTLS() {
		go server.startWithTLS(instance)
	} else {
		go server.startWithoutTLS(instance)
	}
	go func() {
		select {
		case <-server.shutdown:
			instance.Shutdown(nil)
		}
	}()
	<-server.done
}

// Stop kills the server gracefully
func (server *Server) Stop() {
	server.shutdown <- true
}

// addHandlerMiddlewares adds middlewares provided by AddMiddleware
// to the provided handler
func (server *Server) addHandlerMiddlewares(handler *mux.Router) {
	nMiddlewares := len(server.middlewares)
	var augmentedHandler http.Handler = handler
	for i := 0; i < nMiddlewares; i++ {
		augmentedHandler = server.middlewares[i].Handler(augmentedHandler)
	}
}

func (server *Server) addShutdownHandlers(httpServer *http.Server) {
	nShutdownHandlers := len(server.shutdownHandlers)
	for i := 0; i < nShutdownHandlers; i++ {
		httpServer.RegisterOnShutdown(server.shutdownHandlers[i])
	}
	httpServer.RegisterOnShutdown(func() {
		server.logEvent(EventShutdown, "graceful shutdown has completed")
	})
}

// createServerInstance returns a http.Server instance from this Server
// structure
func (server *Server) createServerInstance() *http.Server {
	handler := server.getHandlerWithRoutes()
	server.addHandlerMiddlewares(handler)
	addr := server.GetAddr()
	server.done = make(chan error)
	instance := &http.Server{
		Addr:              addr,
		Handler:           handler,
		MaxHeaderBytes:    server.config.MaxHeaderBytes,
		IdleTimeout:       server.config.TimeoutIdle,
		ReadTimeout:       server.config.TimeoutRead,
		ReadHeaderTimeout: server.config.TimeoutReadHeader,
		WriteTimeout:      server.config.TimeoutWrite,
	}
	server.addShutdownHandlers(instance)
	return instance
}

// getHandlerWithRoutes adds provided handlers via
// AddHandler to the mux instance
func (server *Server) getHandlerWithRoutes() *mux.Router {
	nHandlers := len(server.handlers)
	var handler mux.Router = *server.instance
	for i := 0; i < nHandlers; i++ {
		handler.HandleFunc(server.handlers[i].PathURI, server.handlers[i].Handler)
	}
	return &handler
}

// isTLSEnabled returns whether TLS will be enabled for
// this server
func (server *Server) isTLSEnabled() bool {
	server.logEvent(EventTLSEnabledCheck, "checking if tls should be enabled...")
	isEnabled :=
		utils.FileExists(server.config.TLSKeyPath) &&
			utils.FileExists(server.config.TLSCertPath)
	server.logEvent(EventTLSEnabledCheck, fmt.Sprintf("tls enabled: %v", isEnabled))
	return isEnabled
}

// isTLSValid returns whether TLS configurations are
// valid by loading the key and certificate
func (server *Server) isTLSValid() bool {
	server.logEvent(EventTLSValidityCheck, "checking for tls validity...")
	_, err := tls.LoadX509KeyPair(
		server.config.TLSCertPath,
		server.config.TLSKeyPath,
	)
	isValid := err == nil
	server.logEvent(EventTLSValidityCheck, fmt.Sprintf("tls valid: %v", isValid))
	return isValid
}

// logEvent adds a Server event
func (server *Server) logEvent(code string, message string) {
	if server.Events != nil {
		if len(server.Events) < cap(server.Events) {
			server.Events <- Event{
				Code:    code,
				Message: message,
			}
		}
	}
}

// startWithTLS starts the server instance with TLS enabled (HTTPS)
func (server *Server) startWithTLS(instance *http.Server) {
	server.logEvent(EventStartingWithTLS, fmt.Sprintf("starting tls enabled https server at %s", instance.Addr))
	err := instance.ListenAndServeTLS(
		server.config.TLSCertPath,
		server.config.TLSKeyPath,
	)
	server.logEvent(EventExitted, fmt.Sprintf("server at %s has stopped: '%s'", instance.Addr, err))
	for len(server.Events) > 1 {
		time.After(time.Second)
	}
	server.done <- err
}

// startWithoutTLS starts the server instance with TLS disabled (HTTP)
func (server *Server) startWithoutTLS(instance *http.Server) {
	server.logEvent(EventStartingWithoutTLS, fmt.Sprintf("starting http server at %s", instance.Addr))
	err := instance.ListenAndServe()
	server.logEvent(EventExitted, fmt.Sprintf("server at %s has stopped: '%s'", instance.Addr, err))
	for len(server.Events) > 1 {
		time.After(time.Second)
	}
	server.done <- err
}
